$(function () {
    var user_list_items = $('.user_list_item');

    get('/follows_status', function (response) {
        if (!response.success){
            throw 'Server Error';
        } else {
            for (var i = 0; i < user_list_items.length; i++) {
                var subBtn = findSubscribeBtn(user_list_items, i),
                    unSubBtn = findUnSubscribeBtn(user_list_items, i),
                    user = parseInt($(user_list_items[i]).data('user-id')),
                    parameter = {user: user};

                addListenerToSubBtn(subBtn, parameter, unSubBtn);
                addListenerToUnSubBtn(unSubBtn, parameter, subBtn);

                subBtn.show();

                if (response.followings.length > 0) {
                    $.each(response.followings, function (j, user_id) {
                        if (user === user_id) {
                            subBtn.css('display', 'none');
                            unSubBtn.show();
                        } else {

                        }
                    });
                }
            }
        }
    });
});

function post(route, parameter, action) {
    $.post(route, parameter, action, 'json')
}

function get(route, action) {
    $.get(route, action, 'json')
}

function findSubscribeBtn(users,user) {
    return $(users[user]).find('.subscribe')
}

function findUnSubscribeBtn(users,user) {
    return $(users[user]).find('.unsubscribe')
}

function addListenerToSubBtn(subBtn, parameter, unSubBtn) {
    subBtn.on('click', function () {
        unSubBtn.show();
        subBtn.css('display', 'none');
        post('/follow', parameter, function (response) {
            if (!response.success){
                throw 'Server Error';
            }
        })
    });
}

function addListenerToUnSubBtn(unSubBtn, parameter, subBtn) {
    unSubBtn.on('click', function () {
        subBtn.show();
        unSubBtn.css('display', 'none');
        post('/unsubscribe', parameter, function (response) {
            if (!response.success){
                throw 'Server Error';
            }
        })
    });
}