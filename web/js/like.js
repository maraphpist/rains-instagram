$(function () {
    var photo_list_items = $('.photo_items');

    get('/photo/likes_status', function (response) {
        if (!response.success){
            throw 'Server Error';
        } else {
            for (var i = 0; i < photo_list_items.length; i++) {
                var likeBtn = findSubscribeBtn(photo_list_items, i),
                    dislikeBtn = findUnSubscribeBtn(photo_list_items, i),
                    photo = parseInt($(photo_list_items[i]).data('photo-id')),
                    parameter = { photo: photo };

                addListenerToLikeBtn(likeBtn, parameter, dislikeBtn);
                addListenerToDislikeBtn(dislikeBtn, parameter, likeBtn);

                likeBtn.show();

                if (response.likes.length > 0) {
                    $.each(response.likes, function (j, photo_id) {
                        if (photo === photo_id) {
                            likeBtn.css('display', 'none');
                            dislikeBtn.show();
                        }
                    });
                }
            }
        }
    });

});

function post(route, parameter, action) {
    $.post(route, parameter, action, 'json')
}

function get(route, action) {
    $.get(route, action, 'json')
}

function findSubscribeBtn(photos,photo) {
    return $(photos[photo]).find('.like')
}

function findUnSubscribeBtn(photos,photo) {
    return $(photos[photo]).find('.dislike')
}

function addListenerToLikeBtn(likeBtn, parameter, dislikeBtn) {
    likeBtn.on('click', function () {
        dislikeBtn.show();
        likeBtn.css('display', 'none');
        post('/photo/like', parameter, function (response) {
            if (!response.success){
                throw 'Server Error';
            }
        })
    });
}

function addListenerToDislikeBtn(dislikeBtn, parameter, likeBtn) {
    dislikeBtn.on('click', function () {
        likeBtn.show();
        dislikeBtn.css('display', 'none');
        post('/photo/dislike', parameter, function (response) {
            if (!response.success){
                throw 'Server Error';
            }
        })
    });
}