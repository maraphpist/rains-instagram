<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadArticleData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 5; $i++) {
            $user = new User();
            $user->setUsername('User' . $i)
                ->setEmail('user@user.user' . $i)
                ->setEnabled(true)
                ->setRoles(['ROLE_USER'])
                ->setName('User');
            $encoder = $this->container->get('security.password_encoder');
            $password = $encoder->encodePassword($user, '123321');
            $user->setPassword($password);

            $manager->persist($user);
            $manager->flush();
        }
    }
}
