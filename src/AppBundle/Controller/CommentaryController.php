<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commentary;
use AppBundle\Entity\User;
use AppBundle\Form\CommentaryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CommentaryController extends Controller
{
    /**
     * @Method("POST")
     * @Route("/commentary/{photo_id}")
     * @param Request $request
     * @param int $photo_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newCommentAction(Request $request, int $photo_id)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user){
            $this->redirectToRoute('fos_user_security_login');
        }

        $commentary = new Commentary();

        $commForm = $this->createForm(CommentaryType::class, $commentary);

        $commForm->handleRequest($request);

        $photo = $this->getDoctrine()->getRepository('AppBundle:Photo')
            ->find($photo_id);

        if ($commForm->isValid() && $commForm->isSubmitted()){
            $commentary->setAuthor($user);
            $commentary->setPhoto($photo);
            $commentary->setDatetime(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($commentary);
            $em->flush();
        }

        return $this->redirectToRoute('app_index_photoslist', array(

            ));
    }
}
