<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FollowController extends Controller
{
    /**
     * @Method("POST")
     * @Route("/follow")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function followAction(Request $request)
    {
        $follower = $this->getUser();
        if (!$follower){
            return $this->redirectToRoute('fos_user_security_login');
        }

        try {
            $followed_id = $request->get('user');

            $followed = $this->getDoctrine()
                ->getRepository('AppBundle:User')
                ->find($followed_id);

            $followed->addFollower($follower);
            $follower->addFollowing($followed);

            $em = $this->getDoctrine()->getManager();

            $em->flush();
        } catch (\Exception $e){
            $success = 'Server Error';
            throw $e;
        }


        return new Response(json_encode([
            'success' => $success ?? true
        ]));
    }

    /**
     * @Route("/unsubscribe")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function unsubscribeAction(Request $request){
        /** @var User $follower */
        $follower = $this->getUser();
        if (!$follower){
            return $this->redirectToRoute('fos_user_security_login');
        }
        try {
            $followed_id = $request->get('user');

            $followed = $this->getDoctrine()
                ->getRepository('AppBundle:User')
                ->find($followed_id);

            $follower->removeFollowing($followed);
            $followed->removeFollower($follower);

            $em = $this->getDoctrine()->getManager();
            $em->flush();
        } catch (\Exception $e){
            $success = 'Server Error';
            throw $e;
        }

        return new Response(json_encode([
            'success' => $success ?? true
        ]));
    }

    /**
     * @Method("GET")
     * @Route("/follows_status")
     */
    public function followsStatusAction(){
        /** @var User $follower */
        $follower = $this->getUser();
        if (!$follower){
            return $this->redirectToRoute('fos_user_security_login');
        }
        try {

            $my_followings = $follower->getFollowings();

            $my_followings_id = [];

            foreach ($my_followings as $my_following) {
                $my_followings_id[] = $my_following->getId();
            }
        } catch (\Exception $e) {
            $success = 'Server Error';
            throw $e;
        }

        return new Response(json_encode([
            'followings' => $my_followings_id,
            'success' => $success ?? true
        ]));
    }

}
