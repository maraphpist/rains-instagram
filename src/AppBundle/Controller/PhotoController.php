<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commentary;
use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PhotoController
 * @package AppBundle\Controller
 * @Route("/photo")
 */
class PhotoController extends Controller
{
    /**
     * @Method("delete")
     * @Route("/delete/{photo_id}")
     * @param int $photo_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteOwnPhotoAction(int $photo_id)
    {
        $photo = $this->getDoctrine()->getRepository('AppBundle:Photo')
            ->find($photo_id);
        $commentaries = $photo->getCommentaries();
        $em = $this->getDoctrine()->getManager();
        foreach ($commentaries as $commentary){
            $em->remove($commentary);
        }
        $em->remove($photo);
        $em->flush();

        return $this->redirectToRoute('app_user_index', array(

        ));
    }

    /**
     * @Method("GET")
     * @Route("/show/{photo_id}")
     * @param int $photo_id
     * @return Response
     */
    public function showPhotoAction(int $photo_id){
        $photo = $this->getDoctrine()->getRepository('AppBundle:Photo')
            ->find($photo_id);
        /** @var Commentary[] $commentaries */
        $commentaries = $photo->getCommentaries();
        $dateTime = [];

        foreach ($commentaries as $commentary){
            $dateTime[$commentary->getId()] = $commentary->getDatetime()->format('d.m.Y h:i:s');
        }

        if(count($photo->getLikers()) > 0){
            $likes_count = count($photo->getLikers());
        };

        return $this->render('@App/Photo/showPhoto.html.twig', [
            'photo' => $photo,
            'comments' => $commentaries,
            'datetime' => $dateTime,
            'likes_count' => $likes_count ?? ''
        ]);
    }

    /**
     * @Method("POST")
     * @Route("/like")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function likeAction(Request $request){
        /** @var \AppBundle\Entity\User $liker */
        $liker = $this->getUser();
        if (!$liker){
            return $this->redirectToRoute('fos_user_security_login');
        }

        try {
            $photo_id = $request->get('photo');

            $em = $this->getDoctrine()->getManager();

            $photo = $this->getDoctrine()
                ->getRepository('AppBundle:Photo')
                ->find($photo_id);

            $photo->addLiker($liker);
            $liker->addLikedPhoto($photo);

            $em->flush();
        } catch (\Exception $e) {
            $success = 'Server Error';
            throw $e;
        }

        return new Response(json_encode([
            'success' => $success ?? true
        ]));
    }

    /**
     * @Method("POST")
     * @Route("/dislike")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function dislikeAction(Request $request){
        /** @var \AppBundle\Entity\User $liker */
        $liker = $this->getUser();
        if (!$liker){
            return $this->redirectToRoute('fos_user_security_login');
        }

        try {
            $photo_id = $request->get('photo');

            $photo = $this->getDoctrine()
                ->getRepository('AppBundle:Photo')
                ->find($photo_id);

            $liker->removeLikedPhoto($photo);
            $photo->removeLiker($liker);

            $em = $this->getDoctrine()->getManager();
            $em->flush();
        } catch (\Exception $e){
            $success = 'Server Error';
            throw $e;
        }

        return new Response(json_encode([
            'success' => $success ?? true
        ]));
    }

    /**
     * @Method("GET")
     * @Route("/likes_status")
     */
    public function likeStatusAction(){
        /** @var User $liker */
        $liker = $this->getUser();
        if (!$liker){
            return $this->redirectToRoute('fos_user_security_login');
        }

        try {
            $my_likes = $liker->getLikedphotos();
            $my_likes_id = [];

            foreach ($my_likes as $my_like) {
                $my_likes_id[] = $my_like->getId();
            }
        } catch (\Exception $e) {
            $success = 'Server Error';
            throw $e;
        }

        return new Response(json_encode([
            'likes' => $my_likes_id,
            'success' => $success ?? true
        ]));
    }
}
