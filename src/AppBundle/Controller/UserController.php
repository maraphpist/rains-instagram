<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Form\addPhotoType;
use AppBundle\Form\deletePhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Method({"GET", "POST"})
     * @Route("/")
     */
    public function indexAction()
    {
        $user = $this->getUser();
        if (!$user){
            return $this->redirectToRoute('fos_user_security_login');
        }
        $AddPhotoForm = $this->createForm(addPhotoType::class, null, [
            'action' => $this->generateUrl('app_user_addphoto'),
            'method' => 'POST'
        ]);
        $photos = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->findBy(
                [
                    'author' => $this->getUser()->getId()
                ]
            );

        $deleteForm = [];

        foreach ($photos as $photo){
            $deleteForm[$photo->getId()] = $this->createForm(deletePhotoType::class, null, [
                'action' => $this->generateUrl('app_photo_deleteownphoto', ['photo_id' => $photo->getId()]),
                'method' => 'DELETE'
            ])->createView();
        }
        return $this->render('AppBundle:User:index.html.twig', array(
            'user' => $user,
            'form' => $AddPhotoForm->createView(),
            'photos' => $photos,
            'deleteForm' => $deleteForm
        ));
    }

    /**
     * @Method("POST")
     * @Route("/new_photo")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPhotoAction(Request $request)
    {
        $photo = new Photo();

        $form = $this->createForm(addPhotoType::class, $photo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $photo->setAuthor($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();
        }

        return $this->redirectToRoute('app_user_index');
    }

}
