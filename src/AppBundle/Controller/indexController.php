<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use AppBundle\Form\CommentaryType;
use AppBundle\Form\FollowType;
use AppBundle\Form\LikeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class indexController
 * @package AppBundle\Controller
 * @Route("list")
 */
class indexController extends Controller
{
    /**
     * @Route("/users")
     */
    public function usersListAction()
    {
        if (!$this->getUser()){
            return $this->redirectToRoute('fos_user_security_login');
        }

        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();
        $count = count($users);
        for ($i = 0; $i < $count; $i++) {
            if ($users[$i]->getId() == $this->getUser()->getId()) {
                unset($users[$i]);
            }
        }
        $users = array_values($users);
        $follow_btn = [];

        return $this->render('AppBundle:index:list.html.twig', array(
            'users' => $users,
            'follow_btn' => $follow_btn,
        ));
    }

    /**
     * @Method("GET")
     * @Route("/")
     */
    public function photosListAction(){
        $user = $this->getUser();
        if (!$user){
            $this->redirectToRoute('fos_user_security_login');
        }

        /** @var User[] $followings */
        $followings = $user->getFollowings();
        /** @var Photo[][][] $block */
        $block = [];

        foreach ($followings as $following){
            if (count($following->getOwnedPhotos()) > 0){
                $block['photos'][$following->getId()] = $following->getOwnedPhotos();
            }
        }

        if (isset($block['photos'])){
            foreach ($block['photos'] as $photos_of_user){
                foreach ($photos_of_user as $photo){
                    $block['author'][$photo->getId()] = $photo->getAuthor();
                    if (count($photo->getLikers()) > 0){
                        $block['likes_count'][$photo->getId()] = count($photo->getLikers());
                    } else {
                        $block['likes_count'][$photo->getId()] = '';
                    }

                    $block['commentaryForm'][$photo->getId()] = $this->createForm(CommentaryType::class, null, [
                        'method' => 'POST',
                        'action' => $this->generateUrl('app_commentary_newcomment', ['photo_id' => $photo->getId()])
                    ])->createView();
                }
            }
        } else if (!isset($block['photos'])) {
            $nothing_in_your_list = 'There are nothing to show you in your list!';
        }

        return $this->render('@App/index/photoList.html.twig', [
            'followings' => $followings,
            'user' => $user,
            'block' => $block,
            'message' => $nothing_in_your_list ?? ''
        ]);
    }

}
