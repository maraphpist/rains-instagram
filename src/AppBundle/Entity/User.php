<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $name;


    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="followings")
     */
    protected $followers;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="followers")
     * @ORM\JoinTable(name="follows",
     *      joinColumns={@ORM\JoinColumn(name="follower_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="following_id", referencedColumnName="id")}
     *      )
     */
    protected $followings;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="avatar_file", fileNameProperty="avatar")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="text", nullable=true, unique=false)
     */
    protected $avatar;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Commentary", mappedBy="author")
     */
    protected $commentaries;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Photo", mappedBy="author")
     */
    protected $owned_photos;

    /**
     * @ManyToMany(targetEntity="AppBundle\Entity\Photo", inversedBy="likers")
     * @JoinTable(name="likes",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="photo_id", referencedColumnName="id")}
     *     )
     */
    protected $liked_photos;

    public function __construct()
    {
        parent::__construct();
        $this->followers = new ArrayCollection();
        $this->followings = new ArrayCollection();
        $this->owned_photos = new ArrayCollection();
        $this->liked_photos = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * get avatar
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar(string $avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     *
     * @param File|null $imageFile
     * @return User
     */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * Add follower
     *
     * @param \AppBundle\Entity\User $follower
     *
     * @return User
     */
    public function addFollower(\AppBundle\Entity\User $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param \AppBundle\Entity\User $follower
     */
    public function removeFollower(\AppBundle\Entity\User $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * Add following
     *
     * @param \AppBundle\Entity\User $following
     *
     * @return User
     */
    public function addFollowing(\AppBundle\Entity\User $following)
    {
        $this->followings[] = $following;

        return $this;
    }

    /**
     * Remove following
     *
     * @param \AppBundle\Entity\User $following
     */
    public function removeFollowing(\AppBundle\Entity\User $following)
    {
        $this->followings->removeElement($following);
    }

    /**
     * Get followings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowings()
    {
        return $this->followings;
    }

    /**
     * Add photo
     *
     * @param \AppBundle\Entity\Photo $photo
     *
     * @return User
     */
    public function addPhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->owned_photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \AppBundle\Entity\Photo $photo
     */
    public function removePhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->owned_photos->removeElement($photo);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOwnedPhotos()
    {
        return $this->owned_photos;
    }

    /**
     * Get likes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikedphotos()
    {
        return $this->liked_photos;
    }

    /**
     * Add ownedPhoto
     *
     * @param \AppBundle\Entity\Photo $ownedPhoto
     *
     * @return User
     */
    public function addOwnedPhoto(\AppBundle\Entity\Photo $ownedPhoto)
    {
        $this->owned_photos[] = $ownedPhoto;

        return $this;
    }

    /**
     * Remove ownedPhoto
     *
     * @param \AppBundle\Entity\Photo $ownedPhoto
     */
    public function removeOwnedPhoto(\AppBundle\Entity\Photo $ownedPhoto)
    {
        $this->owned_photos->removeElement($ownedPhoto);
    }

    /**
     * Add likedPhoto
     *
     * @param \AppBundle\Entity\Photo $likedPhoto
     *
     * @return User
     */
    public function addLikedPhoto(\AppBundle\Entity\Photo $likedPhoto)
    {
        $this->liked_photos[] = $likedPhoto;

        return $this;
    }

    /**
     * Remove likedPhoto
     *
     * @param \AppBundle\Entity\Photo $likedPhoto
     */
    public function removeLikedPhoto(\AppBundle\Entity\Photo $likedPhoto)
    {
        $this->liked_photos->removeElement($likedPhoto);
    }

    /**
     * Add commentary
     *
     * @param \AppBundle\Entity\Commentary $commentary
     *
     * @return User
     */
    public function addCommentary(\AppBundle\Entity\Commentary $commentary)
    {
        $this->commentaries[] = $commentary;

        return $this;
    }

    /**
     * Remove commentary
     *
     * @param \AppBundle\Entity\Commentary $commentary
     */
    public function removeCommentary(\AppBundle\Entity\Commentary $commentary)
    {
        $this->commentaries->removeElement($commentary);
    }

    /**
     * Get commentaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentaries()
    {
        return $this->commentaries;
    }
}
